
# README #

!!! IMPORTANT NOTICE !!!

* Version: Extension include scripts only for Contao >=3.2, <=3.6>

### What is this repository for? ###

* Mobile navigation for Contao
* Version: 1.0.1

### How do I get set up? ###

1. Copy folder to */system/modules/*
2. Create Navigation FE-Module and choose as template */system/modules/assets/templates/nav_default_zixMobile.html5*.
3. Create a new page layout and choose only header, main row and footer without left and right column. (if you need left or right column, you can choose this option, but set the column with to 100%).
4. Include Navigation FE-Module in page layout by "included modules" in Header.
5. Set viewport "initial-scale=1.0, user-scalable=no, width=device-width".
6. Choose in page layout jQuery-Templates the direction *j_mobilenavigation-LeftPanel* or *j_mobilenavigation-rightPanel* which direction you wish. 
7. Set Overall with to 100%
8. The pages with only one subpage will not displayed in the mobile navigation, because of this feature you must have a overview page and one subpage. It's mean you need 2 subpages. The Parentpage will redirect to the overview page.

	For example:
	
		Parentpage -> redirect to overviewpage
			overview page
			subpage

9. The pages without subpages will open normaly
10. The extansion remove the some attributes from figure, images, videos and tables. See code below!

		/* Remove attributes and classes */
		$('body').find('.image_container').removeAttr('style').removeClass('float_left float_right float_above');
		$('body').find('.image_container img').removeAttr('width height');
		$('body').find('video').removeAttr('width height');
		$('.ce_form').find('table td').removeAttr('width');

### How it's work? ###

* The files */system/modules/assets/js/MobileNavigation.js*, */system/modules/assets/js/MobileNavigationCore.js*, */system/modules/assets/js/modernizr-2.6.2.min.js* and */system/modules/assets/css/mobilenavigation.css/ or .scss* included automaticaly if device are a Mobile, Android or IOS

* Touchswipes is also included in */system/modules/config/config.php*. If you use any another extension with *jquery.touchSwipe.js* comment this line in *config.php*

* The Navigation include styles till level_3. If you wish more levels you musst style the css file in */system/modules/assets/* for your needs. 


### Who do I talk to? ###

* Hakan Havutcuoglu
* info@havutcuoglu.com

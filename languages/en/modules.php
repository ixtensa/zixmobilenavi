<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package   zixMobileNavi
 * @author    Hakan Havutcuoglu
 * @license   GNU
 * @copyright IXTENSA 2015
 */


/**
 * Back end modules
 */

$GLOBALS['TL_LANG']['MOD']['zixMobileNavi'][0] = 'IXTENSA Smartphone Navigation';
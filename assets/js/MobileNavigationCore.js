/* *
 *
 * @package   zixMobileNavi
 * @authors   Hakan Havutcuoglu
 * @license   GNU LGPL 3+
 * @copyright IXTENSA
 *
 * */

// jQuery
(function($) {

	$(function() {
		var mnav = $('.mnav'), /* menu css class */
			body = $('body'),
			container = $('#wrapper'), /* container css class */
			push = $('.push'), /* css class to add mnav capability */
			siteOverlay = $('.site-overlay'), /* site overlay */
			mnavClass = "mnav-closed mnav-open", /* menu position & menu open class */
			mnavActiveClass = "mnav-active", /* css class to toggle site overlay */
			containerClass = "wrapper-push", /* container open class */
			pushClass = "pushed", /* css class to add mnav capability */
			menuBtn = $('.mnavbar, .mnav a'), /* css classes to toggle the menu */
			menuSpeed = 200, /* jQuery fallback menu speed */
			menuWidth = mnav.width() + "px"; /* jQuery fallback menu width */

		function toggleMnav(){
			body.toggleClass(mnavActiveClass); /* toggle site overlay */
			mnav.toggleClass(mnavClass);
			container.toggleClass(containerClass);
			if(container.hasClass(containerClass)){
				container.css({"position": "fixed"});
			} else {
				container.removeAttr("style");
			}
			push.toggleClass(pushClass); /* css class to add mnav capability */
		}

		function openMnavFallback(){
			body.addClass(mnavActiveClass);
			mnav.animate({left: "0px"}, menuSpeed);
			container.animate({left: menuWidth}, menuSpeed);
			push.animate({left: menuWidth}, menuSpeed); /* css class to add mnav capability */
		}

		function closeMnavFallback(){
			body.removeClass(mnavActiveClass);
			mnav.animate({left: "-" + menuWidth}, menuSpeed);
			container.animate({left: "0px"}, menuSpeed);
			push.animate({left: "0px"}, menuSpeed); /* css class to add mnav capability */
		}

		if(Modernizr.csstransforms3d){
			/* toggle menu */
			menuBtn.click(function() {
				toggleMnav();
			});
			/* close menu when clicking site overlay */
			siteOverlay.click(function(){ 
				toggleMnav();
			});
		}else{
			/* jQuery fallback */
			mnav.css({left: "-" + menuWidth}); /* hide menu by default */
			container.css({"overflow-x": "hidden"}); /* fixes IE scrollbar issue */

			/* keep track of menu state (open/close) */
			var state = true;

			/* toggle menu */
			menuBtn.click(function() {
				if (state) {
					openMnavFallback();
					state = false;
				} else {
					closeMnavFallback();
					state = true;
				}
			});

			/* close menu when clicking site overlay */
			siteOverlay.click(function(){ 
				if (state) {
					openMnavFallback();
					state = false;
				} else {
					closeMnavFallback();
					state = true;
				}
			});
		}
	});

})(jQuery);

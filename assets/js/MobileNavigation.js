/* *
 *
 * @package   zixMobileNavi
 * @authors   Hakan Havutcuoglu & Fabian Perrey
 * @license   GNU LGPL 3+
 * @copyright IXTENSA
 *
 * */

// jQuery
(function($) {

	$(document).ready(function() {
		
		/* Remove attributes and classes */
		$('body').find('.image_container').removeAttr('style').removeClass('float_left float_right float_above');
		$('body').find('.image_container img').removeAttr('width height');
		$('body').find('video').removeAttr('width height');
		$('.ce_table').find('th, td').removeAttr('width');
		$('.ce_text').find('table td').removeAttr('width');
		
		/* Show active menu by page reload */
		animateNav();
		$('#navi-mobile li.submenu.open > ul').slideDown('fast');
	});

	/* Core function of Navigation */
	function animateNav() {
		var clickBtns = $('#navi-mobile .clickBtn');
		/* clickBtns.unbind('click'); */

		clickBtns.click(function (event) {

			event.stopPropagation();

			/* if menulink open, close it */
			if ($(this).parent().hasClass('open')) {
				$(this).siblings('ul').slideUp('fast');
				$(this).parent().removeClass('open');

			/* else close open menulink and open clicken menulink */
			} else {
				if (!$(this).parent('li').parent('ul').parent('li').hasClass('open')) {
					$('#navi-mobile .open > ul').slideUp('fast');
					$('#navi-mobile .open').removeClass('open');
				} else {
					$(this).parent('li').siblings('li').removeClass('open').children('ul').slideUp('fast');
				}
				$(this).parent().addClass('open');
				$(this).siblings('ul').slideDown('fast', function() {
					$('html, body').animate({
						scrollTop: $(this).siblings('.clickBtn').offset().top
					}, 400);
				});
			}
		});
	}
	
})(jQuery);
<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2013 Leo Feyer
 *
 * @package   zixMobileNavi
 * @author    Hakan Havutcuoglu
 * @license   GNU
 * @copyright IXTENSA
 */

/**
 * Register the namespace
 */
ClassLoader::addNamespace('IXTENSA');


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	// Classes
	'IXTENSA\checkSwipeJs' => 'system/modules/zixMobileNavi/classes/checkSwipeJs.php',
));

/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
	'nav_default_zixMobile' => 'system/modules/zixMobileNavi/templates', // Load navigation template for mobile
	//'fe_page_zixMobile' => 'system/modules/zixMobileNavi/templates', // Load fe_page template for mobile
	'j_mobilenavigation-LeftPanel' => 'system/modules/zixMobileNavi/templates', // Overlay and position LeftPanel
	'j_mobilenavigation-RightPanel' => 'system/modules/zixMobileNavi/templates', // Overlay and position RightPanel

));
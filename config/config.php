<?php
/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2013 Leo Feyer
 *
 * @package   zixMobileNavi
 * @author    Hakan Havutcuoglu
 * @license   GNU
 * @copyright IXTENSA
 */


/*
 * Hooks
 */

// generatePage addSwipeJs
$GLOBALS['TL_HOOKS']['generatePage'][] = array('checkSwipeJs', 'addSwipeJs' );



if (TL_MODE == 'FE') {
	// Check if it's mobile
	$objEnvironment = Environment::getinstance();
	$ua = $objEnvironment->agent;
	 
	//echo $ua->os;      // Operation System
	//echo $ua->browser; // Browser name
	//echo $ua->shorty;  // Browser shortcut
	//echo $ua->version; // Browser version
	//echo $ua->mobile;  // True if the Client a mobile browser
	//echo $ua->class;   // CSS class-string

	if ($ua->mobile) {
		// JS
		$GLOBALS['TL_JAVASCRIPT'][] = 'system/modules/zixMobileNavi/assets/js/modernizr-2.6.2.min.js|static'; // JS for old devices
		$GLOBALS['TL_JAVASCRIPT'][] = 'system/modules/zixMobileNavi/assets/js/MobileNavigationCore.js|static'; // JS for panel function
		$GLOBALS['TL_JAVASCRIPT'][] = 'system/modules/zixMobileNavi/assets/js/MobileNavigation.js|static'; // JS for navi slide down-up

		// CSS
		$GLOBALS['TL_CSS'][] = 'system/modules/zixMobileNavi/assets/css/mobilenavigation.scss|static'; // CSS for MobileNavigation
	
	}
}
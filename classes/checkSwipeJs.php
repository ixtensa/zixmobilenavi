<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package   checkSwipeJs
 * @author    Hakan Havutcuoglu
 * @license   GNU
 * @copyright IXTENSA 2015
 */

/**
 * Namespace
 */

namespace IXTENSA;


class checkSwipeJs extends \Frontend
{
	/**
	 * add jQuery.touchSwipe.min.js to head
	 */
	public function addSwipeJs($objPage, $objLayout, $self)
	{
		if (isset($GLOBALS['BE_MOD']['content']['caroufredsel'])) {
			//include touch/swipe support
			$GLOBALS['TL_JAVASCRIPT'][] = 'system/modules/dk_caroufredsel/assets/js/jquery.touchSwipe.min.js|static'; // JS for touch Gestures
		} else {
			// include touch/swipe support
			$GLOBALS['TL_JAVASCRIPT'][] = 'system/modules/zixMobileNavi/assets/js/jquery.touchSwipe.min.js|static'; // JS for touch Gestures
		}
	}
}